function getBotResponse(input) {
    //rock paper scissors
    input = input.toLowerCase()

    if (input == "rock") {
        return "paper";
    } else if (input == "paper") {
        return "scissors";
    } else if (input == "scissors") {
        return "rock";
    }
    
    if (input == "..."){
        return "I didn't understand. Try asking questions like, 'When is the due date?', 'What is this website about?', or even simple, one word questions like, 'What?' and 'About?'. I will try my best to interpret your input.";
    }
    
    // Simple responses
    if (input == "hello") {
        return "Hello there!";
    } else if (input == "goodbye") {
        return "Talk to you later!";
    } 
    
    if (input.split(" ").includes("what")) {
        if (input.includes("about") || input.includes("event") || input.includes("for")) {
            return "Answer A."
        } else if (input.includes("goal") || input.includes("mission")) {
            return "Answer B."
        } else{
            return "Please be more specific. Are you asking what this website is about? Or what our goals are?"
        }
    }

    if (input.split(" ").includes("when")) {
        if (input.includes("event") && input.includes("due") || input.includes("due")) {
            return "Answer C."
        } else if (input.includes("apply") || input.includes("sign")) {
            return "Answer D."
        } else{
            return "Please be more specific. Are you asking for when the competition starts? Or when you can sign up?"
        }
    }

    if (input.split(" ").includes("how")) {
        if (input.includes("sign") && input.includes("up") || input.includes("sign") || input.includes("apply")) {
            return "Answer E."
        } else if (input.includes("winner") || input.includes("end")) {
            return "Answer F."
        } else{
            return "Please be more specific. Are you asking for how to sign up? Or how the competition ends?"
        }
    }

    if (input.split(" ").includes("who")) {
        if (input.includes("can") && input.includes("compete") || input.includes("compete")) {
            return "Answer G."
        } else if (input.includes("design") || input.includes("you")) {
            return "Answer H."
        } else{
            return "Please be more specific. Are you asking for who can compete? Or who designed this website?"
        }
    }

    if (input.split(" ").includes("can")) {
        if (input.includes("work") && input.includes("multiple") || input.includes("work") && input.includes("more")) {
            return "Answer I."
        } else{
            return "Please be more specific."
        }
    }
    

    else {
        return "I didn't understand. Try asking questions like, 'When is the due date?', 'What is this website about?', or even simple, one word questions like, 'What?' and 'About?'. I will try my best to interpret your input.";
    }

    

}